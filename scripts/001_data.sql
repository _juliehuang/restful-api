CREATE DATABASE api;
\c api;
CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	name VARCHAR(30),
	email VARCHAR(30)
);
INSERT INTO users (name, email)
	VALUES ('Jon', 'jon@email.com'), ('Patty', 'patty@email.com');

