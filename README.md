# My Contacts App 
A web app to manage your contacts list. APi is running on an Express framework in Node.js utilizing a PostgreSQL
database. The frontend is served using Vue.js

The objective of this project was to learn how to build a fullstack app complete with a database using JavaScript and to
gain some exposure to Vue.js. 

## Architecture
```
  Database                          Backend
+---------+             |-----------------------------+              Frontend
|         |             |                             |          +--------------+
|         |             |  +--------+     +--------+  |          |              |
| Postgres+------------>+  |        |     |        |  +--------->+              |
|         |             |  | Node.js|     | Express|  |          |     Vue      |
|         +<------------+  |        |     |        |  +<---------+              |
|         |             |  |        |     |        |  |          |              |
|         |             |  +--------+     +--------+  |          +--------------+
|         |             |                             |
+---------+             +-----------------------------+

```
## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes.

### Prerequisites
- Node.js & npm
- PostgrSQL
- Docker
- Vue.js

### Development
You will need to export the necessary environment variables as outlined in the `docker-compose.yml` file. There are a
few ways you can do this: 
1. Put all environment variables into a file and run the following to export each variable in the file `export $(egrep
   -v '^#' <FILENAME>.env | xargs)`
2. Export each environment variable of the following environment variables directly in the command line. 
```
POSTGRES_USER=username
POSTGRES_PASSWORD=password
POSTGRES_HOST=database
POSTGRES_DB=api
POSTGRES_PORT=5432
API_PORT=3000
```
Once that is all done, to spin up the containers, run `docker-compose up` in root directory of the project. 

You should be able to navigate to http://localhost:8081 and see the users that are preloaded. 
